import SlimSelect from 'slim-select'

$.each('.js-select', el => {
    new SlimSelect({
        select: el,
        showSearch: false,
        showContent: 'down',
        hideSelectedOption: true
    })
})