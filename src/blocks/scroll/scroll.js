import LocomotiveScroll from 'locomotive-scroll';

let loco;

window.addEventListener('DOMContentLoaded', () => {
  const scrollContainer = $.qs('#js-scroll');
  if (!scrollContainer) return false;

  // Scroll show/hide
  const scrollbar = {
    show: () => {
      if (loco.scroll.scrollbar)
        loco.scroll.scrollbar.classList.remove('u-hidden');
    },
    hide: () => {
      if (loco.scroll.scrollbar)
        loco.scroll.scrollbar.classList.add('u-hidden');
    }
  };

  // Scroll instance
  loco = new LocomotiveScroll({
    el: scrollContainer,
    smooth: true,
    getDirection: true
  });

  window.loco = loco;

  // Scroll data, start/stop functions
  window.scroll = {
    data: {
      direction: 'down',
    },
    start: () => {
      window.addEventListener('keydown', loco.scroll.checkKey, false);
      scrollbar.show();
      loco.start();
    },
    stop: () => {
      window.removeEventListener('keydown', loco.scroll.checkKey, false);
      scrollbar.hide();
      loco.stop();
    }
  };

  // Save scroll data
  loco.on('scroll', e => {
    window.scroll.data = e;
    document.dispatchEvent(new CustomEvent("customScroll", {
      detail: {
        offsetY: e.scroll.y,
        direction: e.direction
      }
    }));
  });
});

// Update scroll on full page load
window.addEventListener('load', () => {
  if (loco) loco.update();
});
