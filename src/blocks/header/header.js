
const header = $.qs("header");
if(header){
    document.addEventListener('customScroll', function(event) { 

        // Drop active class
        if (event.detail.offsetY >= header.offsetHeight) {
            header.classList.add("is-active")
        }
        else {
            header.classList.remove("is-active")
        }
        
        // Drop direction class
        if (event.detail.direction == "down") {
            header.classList.add("is-down")
        }
        else {
            header.classList.remove("is-down")
        }
    });
};
