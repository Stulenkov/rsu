import ScrollBooster from 'scrollbooster';

$.each('.js-booster-x', el => {
  new ScrollBooster({
    viewport: el,
    content: el.querySelector('.js-booster__inner'),
    scrollMode: 'transform',
    direction: 'horizontal',
    emulateScroll: false, 
  });
})

$.each('.js-booster-mobile-x', el => {
  if(window.innerWidth <= 1100){
    new ScrollBooster({
      viewport: el,
      content: el.querySelector('.js-booster__inner'),
      scrollMode: 'transform',
      direction: 'horizontal',
      emulateScroll: false, 
    });
  }
})

$.each('.js-booster-y', el => {
  new ScrollBooster({
    viewport: el,
    content: el.querySelector('.js-booster__inner'),
    scrollMode: 'transform',
    direction: 'vertical',
    emulateScroll: false, 
  });
})

$.each('.js-booster-desktop-y', el => {
  if(window.innerWidth >= 1100){
    new ScrollBooster({
      viewport: el,
      content: el.querySelector('.js-booster__inner'),
      scrollMode: 'transform',
      direction: 'vertical',
      emulateScroll: false, 
    });
  }
})